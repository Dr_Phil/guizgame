//
//  ViewController.m
//  QuizGame
//
//  Created by Filip Winbo on 2016-02-02.
//  Copyright © 2016 Filip Winbo. All rights reserved.
//

#import "ViewController.h"
#import "GameLogic.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *Question;
@property (weak, nonatomic) IBOutlet UILabel *A;
@property (weak, nonatomic) IBOutlet UILabel *B;
@property (weak, nonatomic) IBOutlet UILabel *C;
@property (weak, nonatomic) IBOutlet UILabel *D;
@property (weak, nonatomic) IBOutlet UILabel *Result;
@property (strong, nonatomic) GameLogic *gameLogic;
@property (nonatomic) NSArray *currentQuestion;
@end

@implementation ViewController

-(GameLogic*)gameLogic {
    if (!_gameLogic) {
        _gameLogic = [[GameLogic alloc] init];
    }
    return _gameLogic;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.currentQuestion = [self.gameLogic getQuestion];
    self.Question.text = self.currentQuestion[0];
    self.A.text = self.currentQuestion[1];
    self.B.text = self.currentQuestion[2];
    self.C.text = self.currentQuestion[3];
    self.D.text = self.currentQuestion[4];
}
- (IBAction)AnswerA:(id)sender {
   self.Result.text = [self.gameLogic returnAnswer:@"A"];
    self.currentQuestion = [self.gameLogic getQuestion];
    self.Question.text = self.currentQuestion[0];
    self.A.text = self.currentQuestion[1];
    self.B.text = self.currentQuestion[2];
    self.C.text = self.currentQuestion[3];
    self.D.text = self.currentQuestion[4];
}
- (IBAction)AnswerB:(id)sender {
   self.Result.text = [self.gameLogic returnAnswer:@"B"];
    self.currentQuestion = [self.gameLogic getQuestion];
    self.Question.text = self.currentQuestion[0];
    self.A.text = self.currentQuestion[1];
    self.B.text = self.currentQuestion[2];
    self.C.text = self.currentQuestion[3];
    self.D.text = self.currentQuestion[4];
}
- (IBAction)AnswerC:(id)sender {
   self.Result.text = [self.gameLogic returnAnswer:@"C"];
    self.currentQuestion = [self.gameLogic getQuestion];
    self.Question.text = self.currentQuestion[0];
    self.A.text = self.currentQuestion[1];
    self.B.text = self.currentQuestion[2];
    self.C.text = self.currentQuestion[3];
    self.D.text = self.currentQuestion[4];
}
- (IBAction)AnswerD:(id)sender {
   self.Result.text = [self.gameLogic returnAnswer:@"D"];
    self.currentQuestion = [self.gameLogic getQuestion];
    self.Question.text = self.currentQuestion[0];
    self.A.text = self.currentQuestion[1];
    self.B.text = self.currentQuestion[2];
    self.C.text = self.currentQuestion[3];
    self.D.text = self.currentQuestion[4];
}
@end
