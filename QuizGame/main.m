//
//  main.m
//  QuizGame
//
//  Created by Filip Winbo on 2016-02-02.
//  Copyright © 2016 Filip Winbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
