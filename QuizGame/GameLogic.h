//
//  GameLogic.h
//  QuizGame
//
//  Created by Filip Winbo on 2016-02-03.
//  Copyright © 2016 Filip Winbo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameLogic : NSObject

-(NSArray*) getQuestion;

-(NSString*) returnAnswer:(NSString*)answer;

@end
