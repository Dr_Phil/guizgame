//
//  GameLogic.m
//  QuizGame
//
//  Created by Filip Winbo on 2016-02-03.
//  Copyright © 2016 Filip Winbo. All rights reserved.
//

#import "GameLogic.h"

@interface GameLogic()
@property (nonatomic) NSArray *Q1;
@property (nonatomic) NSArray *Q2;
@property (nonatomic) NSArray *Q3;
@property (nonatomic) NSArray *Q4;
@property (nonatomic) NSArray *Q5;
@property (nonatomic) NSArray *Q6;
@property (nonatomic) NSArray *Q7;
@property (nonatomic) NSArray *Q8;
@property (nonatomic) NSArray *Q9;
@property (nonatomic) NSArray *Q10;
@property (nonatomic) NSArray *rightAnswers;
@property (nonatomic) NSArray *questions;
@end

@implementation GameLogic

-(instancetype) init {
    self = [super init];
    if (self) {
        self.Q1 = @[@"Kvadratroten ur 36", @"A 5", @"B 6", @"C 7", @"D 8"];
        self.Q2 = @[@"Det tyska ordet kartoffel betyder:", @"A Toffla", @"B Stol", @"C Potatis", @"D Skridsko"];
        self.Q3 = @[@"Hur ofta är det val i Sverige?", @"A Vart 4:e år", @"B Varje år", @"C vartannat år", @"D Vart 5:e år"];
        self.Q4 = @[@"Berlinmuren föll år:", @"A 1989", @"B 1986", @"C 2002", @"D 1999"];
        self.Q5 = @[@"Nirvanas sångare", @"A Freddy Mercury", @"B Dave Growl", @"C Lemmy Kilmister", @"D Curt Cobain"];
        self.Q6 = @[@"Engelskans vanligaste ord", @"A The", @"B On", @"C And", @"D In"];
        self.Q7 = @[@"Larmnummret i Sverige", @"A 11414", @"B 90000", @"C 112", @"D 911"];
        self.Q8 = @[@"Maxhastighet i gångfartsområde", @"A 10km/h", @"B 5km/h", @"C 15km/h", @"D 7km/h"];
        self.Q9 = @[@"Vart hölls sommar-OS år 2000?", @"A Athén", @"B Sidney", @"C Berlin", @"D Tokyo"];
        self.Q10 = @[@"Coca-Cola kommer från delstaten:", @"A Ohio", @"B Alabama", @"C Californien", @"D Georgia"];
        self.rightAnswers = @[@"B", @"C", @"A", @"A", @"D", @"A", @"C", @"D", @"B", @"D"];
        self.questions = @[self.Q1, self.Q2, self.Q3, self.Q4, self.Q5, self.Q6, self.Q7, self.Q8, self.Q9, self.Q10];
    }
    return self;
}

int currentQuestion;

-(NSArray*) getQuestion {
    currentQuestion = 1 + arc4random() % self.questions.count;
    
    return self.questions[currentQuestion];
}

-(NSString*) returnAnswer:(NSString*)answer {
    NSString *rightAnswer = self.rightAnswers[currentQuestion];
    if ([answer isEqualToString:rightAnswer]) {
        return @"Rätt!";
    }
    else {
        return @"Fel!";
    }
}

@end
